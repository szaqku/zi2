package pl.oj44447.ZI2;

import pl.oj44447.ZI2.entities.l2.*;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        final EntityManagerFactory emf = Persistence.createEntityManagerFactory("pUnit");
        var em = emf.createEntityManager();

        PersonalData[] teachersData = new PersonalData[] {
                new PersonalData("Jan", "Polak", 43),
                new PersonalData("Anna", "Niemiec", 25),
                new PersonalData("Tomasz", "Rusek", 31)
        };

        Student[] classOneStudents = new Student[]{
                new Student(new PersonalData("Jan", "Kowalski", 12)),
                new Student(new PersonalData("Anna", "Kowalski", 12)),
                new Student(new PersonalData("Tomasz", "Kowalski", 13)),
                new Student(new PersonalData( "Grzegorz", "Kowalski", 12)),
                new Student(new PersonalData( "Maciej", "Kowalski", 12)),
        };

        Student[] classTwoStudents = new Student[]{
                new Student(new PersonalData("Jan", "Nowak", 12)),
                new Student(new PersonalData("Anna", "Nowak", 12)),
                new Student(new PersonalData("Tomasz", "Nowak", 13)),
                new Student(new PersonalData( "Grzegorz", "Nowak", 12)),
                new Student(new PersonalData( "Maciej", "Nowak", 12)),
        };

        Student[] classTreeStudents = new Student[]{
                new Student(new PersonalData("Jan", "Doe", 12)),
                new Student(new PersonalData("Anna", "Doe", 12)),
                new Student(new PersonalData("Tomasz", "Doe", 13)),
                new Student(new PersonalData( "Grzegorz", "Doe", 12)),
                new Student(new PersonalData( "Maciej", "Doe", 12)),
        };

        em.getTransaction().begin();

        SchoolSubject art = new SchoolSubject(null, "Art", null);
        SchoolSubject biology = new SchoolSubject(null, "Biology", null);
        SchoolSubject chemistry = new SchoolSubject(null, "Chemistry", null);
        SchoolSubject english = new SchoolSubject(null, "English", null);

        Teacher teacherOne = new Teacher(null, teachersData[0],null, List.of(art, biology, chemistry, english));

        art.setLeader(teacherOne);
        biology.setLeader(teacherOne);
        chemistry.setLeader(teacherOne);
        english.setLeader(teacherOne);

        em.persist(teacherOne);
        em.flush();

        SchoolClass classOne = new SchoolClass(null, "Class One", 6, teacherOne,
                List.of(art, biology, chemistry, english),
                Arrays.asList(classOneStudents));

        em.persist(classOne);
        em.flush();

        SchoolSubject geography = new SchoolSubject(null, "Geography", null);
        SchoolSubject german = new SchoolSubject(null, "German", null);
        SchoolSubject history = new SchoolSubject(null, "History", null);
        SchoolSubject ict = new SchoolSubject(null, "ICT", null);

        Teacher teacherTwo = new Teacher(null, teachersData[1],null, List.of(geography, german, history, ict));

        geography.setLeader(teacherTwo);
        german.setLeader(teacherTwo);
        history.setLeader(teacherTwo);
        ict.setLeader(teacherTwo);

        em.persist(teacherTwo);
        em.flush();

        SchoolClass classTwo = new SchoolClass(null, "Class Two", 6, teacherTwo,
                List.of(geography, german, history, ict),
                Arrays.asList(classTwoStudents));

        em.persist(classTwo);
        em.flush();

        SchoolSubject italian = new SchoolSubject(null, "Italian", null);
        SchoolSubject russian = new SchoolSubject(null, "Russian", null);
        SchoolSubject maths = new SchoolSubject(null, "Maths", null);
        SchoolSubject music = new SchoolSubject(null, "Music", null);

        Teacher teacherTree = new Teacher(null, teachersData[2],null,
                List.of(italian, russian, maths, music));

        italian.setLeader(teacherTree);
        russian.setLeader(teacherTree);
        maths.setLeader(teacherTree);
        music.setLeader(teacherTree);

        em.persist(teacherTree);
        em.flush();

        SchoolClass classTree = new SchoolClass(null, "Class Tree", 6, teacherTree,
                List.of(italian, russian, maths, music),
                Arrays.asList(classTreeStudents));

        em.persist(classTree);
        em.flush();

        em.getTransaction().commit();


        System.out.println("Przedmioty ucznia o id 1: ");
        var student = (Student) em.createQuery("SELECT student FROM Student student where student.id = 1").getSingleResult();
        student.getSchoolClass().getSchoolSubjects().forEach(System.out::println);

        System.out.println("Uczniowie nauczyciela o id 1: ");
        var students = (List<Student>) em.createQuery("SELECT distinct student FROM Student student " +
                "inner join student.schoolClass schoolClass where schoolClass.teacher.id = 1").getResultList();

        students.forEach(System.out::println);

    }

}

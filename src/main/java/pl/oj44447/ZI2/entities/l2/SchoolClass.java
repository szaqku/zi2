package pl.oj44447.ZI2.entities.l2;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class SchoolClass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Integer level;

    @OneToOne(cascade = CascadeType.MERGE)
    private Teacher teacher;

    @OneToMany
            (cascade = CascadeType.PERSIST)
    private List<SchoolSubject> schoolSubjects;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Student> students;

    @PrePersist
    private void prePersist() {
        if (students != null)
            this.students.forEach(student -> student.setSchoolClass(this));

        if (teacher != null) {
            this.teacher.setSchoolClass(this);
        }
    }
}

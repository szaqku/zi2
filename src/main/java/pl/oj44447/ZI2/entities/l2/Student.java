package pl.oj44447.ZI2.entities.l2;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;

    @Embedded
    private PersonalData personalData;

    @ManyToOne
    private SchoolClass schoolClass;

    public Student(PersonalData personalData) {
        this.personalData = personalData;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", personalData=" + personalData +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id.equals(student.id) &&
                personalData.equals(student.personalData) &&
                Objects.equals(schoolClass, student.schoolClass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, personalData, schoolClass);
    }
}

package pl.oj44447.ZI2.entities.l2;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Embedded
    private PersonalData personalData;

    @OneToOne
    private SchoolClass schoolClass;

    @OneToMany
            (cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<SchoolSubject> schoolSubjects;

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", personalData=" + personalData +
                ", schoolClass=" + schoolClass.getName() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teacher teacher = (Teacher) o;
        return id.equals(teacher.id) &&
                personalData.equals(teacher.personalData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, personalData);
    }
}

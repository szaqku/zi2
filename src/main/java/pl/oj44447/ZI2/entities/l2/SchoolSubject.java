package pl.oj44447.ZI2.entities.l2;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SchoolSubject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;

    private String name;

    @OneToOne
    private Teacher leader;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SchoolSubject that = (SchoolSubject) o;
        return id.equals(that.id) &&
                name.equals(that.name) &&
                Objects.equals(leader, that.leader);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, leader);
    }
}

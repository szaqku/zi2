package pl.oj44447.ZI2.entities.l2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
@EqualsAndHashCode

public class PersonalData {

    private String firstName;
    private String lastName;
    private Integer age;
}

<?php

$user = "my_user";
$pass = "-";

$znakowe = [
    "Lorem",
    "ipsum",
    "dolor",
    "sit amet",
    "consectetur",
    "adipiscing",
    "elit",
    "Vivamus",
    "faucibus",
    "davinci"
];

$isTransactionActive = false;

try {
    $pdo = new PDO("mysql:host=51.38.112.223;port=6603;dbname=testowa", $user, $pass);
    $isTransactionActive = $pdo->beginTransaction();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->exec("INSERT INTO tabelka VALUES (null, 'z1', 123)");
    $pdo->exec("INSERT INTO tabelka VALUES (null, 'z2', 234)");

    foreach ($znakowe as $el) {
        $statement = $pdo->prepare("INSERT INTO tabelka VALUES (null, ?, ?)");
        $el2 = rand(1, 100);
        if ($statement->execute([$el, $el2])) {
            print "Successfully inserted new record: $el, $el2\n";
        }
    }

    $pdo->commit();
} catch(PDOException $ex) {
    if ($isTransactionActive) {
        $pdo->rollBack();
        print "Rollback !!!\n";
    }
    echo 'Connection failed: ' . $ex->getMessage();
}

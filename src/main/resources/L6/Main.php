<?php


require_once __DIR__ . '/../vendor/autoload.php';
use Doctrine\DBAL\DriverManager;

$connectionParams = array(
    'dbname' => 'testowa4',
    'user' => 'root',
    'password' => '',
    'host' => 'localhost',
    'port' => '6603',
    'driver' => 'pdo_mysql'
);

$conn = DriverManager::getConnection($connectionParams);

$queryBuilder = $conn->createQueryBuilder();

// 4.1

$queryBuilder->select('*')
    ->from('customers')
    ->where('country = "USA"');

$stmt = $queryBuilder->execute();
$result = $stmt->fetchAllAssociative();

foreach($result as $row) {
    echo implode(' | ', $row) . "\n";
}

echo "\n\n\n\n";
// 4.2
$queryBuilder = $conn->createQueryBuilder();

$queryBuilder->select('c.customerNumber, c.customerName, e.firstName, e.lastName')
    ->from('customers', 'c')
    ->innerJoin('c', 'employees', 'e', 'c.salesRepEmployeeNumber = e.employeeNumber');

$stmt = $queryBuilder->execute();
$result = $stmt->fetchAllAssociative();

foreach($result as $row) {
    echo implode(' | ', $row) . "\n";
}

// 5.

$schema = new \Doctrine\DBAL\Schema\Schema();
$table = $schema->createTable('myTable');
$table->addColumn('id', 'integer', ["unsigned" => true]);
$table->addColumn('napis', 'string');
$table->addColumn('liczba', 'integer');

$table->setPrimaryKey(['id']);

$conn->executeStatement($schema->toSql($conn->getDatabasePlatform())[0]);


// 6.
$queryBuilder = $conn->createQueryBuilder();

for ($i = 0; $i < 10; $i++){
$queryBuilder
        ->insert('myTable')
        ->values(
            array(
                'id' => '?',
                'napis' => '?',
                'liczba' => '?'
            )
        )
        ->setParameter(0, "$i")
        ->setParameter(1, "napis$i")
        ->setParameter(2, 100+$i)
    ;
    $stmt = $queryBuilder->execute();
}
